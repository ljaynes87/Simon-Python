import tkinter as tk
from tkinter import ttk
from random import randint
from time import sleep
import tkinter.messagebox
import tkinter.simpledialog
import collections
LARGE_FONT =("Verdana",12)
gameList = []
userCount = 0
start = 0
buttonNumber = 5
startButt = False
best = []
players = ""
name = ""
speed = .5
d = {}

## Create sorted list of highscores from a dictionary    
def highScores():
    
    global d
    global players
    players = " "
    global best
    with open("Hi.txt") as f:
        for line in f:
            (val, key) = line.split()
            d[val] = int(key)
    Player = collections.namedtuple('Player', 'score name')
    best = sorted([Player(v,k) for (k,v) in d.items()], reverse = True)
    for i in range(9):
        players += "\n" + str(best[i].score)+ " " +best[i].name



class Window(tk.Tk):
    
#Initialization  of the heavyweight container frame
    def __init__(self,*args, **kwargs):
        tk.Tk.__init__(self,*args, **kwargs)

        container = tk.Frame(self)        
        container.pack(side="top", fill = "both", expand = True)        
        container.grid_rowconfigure(0, weight = 1)
        container.grid_columnconfigure(0, weight = 1)

        self.frames = {}

        ##Add every new page to frame dictionary
        for F in (StartPage,PageOne,HighScore,Rules):

            frame = F(container, self)

            self.frames[F] = frame
            
            frame.grid(row = 0, column = 0, sticky = "nsew")

        self.show_frame(StartPage)


        ##Set size of window
        self.title('Simon')
        w = 560
        h = 579
        sw = self.winfo_screenwidth()
        sh = self.winfo_screenheight()
        x = (sw - w)/2
        y = (sh - h)/2
        self.geometry('%dx%d+%d+%d' % (w, h, x, y))


    def show_frame(self,cont):
        frame = self.frames[cont]
        frame.tkraise()


class StartPage(tk.Frame):
    
    def __init__(self,parent,controller):
        tk.Frame.__init__(self,parent)



        """Add image to background"""
        # pick a .gif image file you have in the working directory
        image1 = tk.PhotoImage(file="ba.png")
        w = image1.width()
        h = image1.height()
        # tk.Frame has no image argument
        # so use a label as a panel/frame
        panel1 = tk.Label(self, image=image1)
        panel1.pack(side='top', fill='both', expand='yes')
        # save the panel's image from 'garbage collection'
        panel1.image = image1


        label = ttk.Label(panel1,text = "Simon", font = ("Veranda",30))
        label.pack(pady=10,padx=10)

        button = ttk.Button(panel1, text = "Play Game", command=lambda: controller.show_frame(PageOne))
        button.pack(side='top')
        hiButton = ttk.Button(panel1, text = "High Scores",command = lambda: controller.show_frame(HighScore))
        hiButton.pack(side='top')
        rulesButton = ttk.Button(panel1, text = "Rules",command = lambda: controller.show_frame(Rules))
        rulesButton.pack(side='top')



      
class HighScore(tk.Frame):
    
    def __init__(self,parent,controller):
        
        highScores()
        tk.Frame.__init__(self,parent)

        """Add image to background"""
        # pick a .gif image file you have in the working directory
        image1 = tk.PhotoImage(file="ba.png")
        w = image1.width()
        h = image1.height()
        # tk.Frame has no image argument
        # so use a label as a panel/frame
        panel1 = tk.Label(self, image=image1)
        panel1.pack(side='top', fill='both', expand='yes')
        # save the panel's image from 'garbage collection'
        panel1.image = image1

        ##Create a scores variable to hold the high scores
        self.var = tk.StringVar()
        self.var.set(players)
        self.scores = tk.Label(panel1, textvariable = self.var, font = ("Verdana",24) )
        self.scores.pack()
            
        self.refreshButt = ttk.Button(panel1, text = "Refresh", command = self.update)
        self.refreshButt.pack()

        backButton = ttk.Button(panel1, text = "Back" , command = lambda: controller.show_frame(StartPage) )
        backButton.pack()

    ##update the the high scores
    def update(self):
        global players
        highScores()
        self.var.set(players)

        
        



class PageOne(tk.Frame):

    def __init__(self,parent,controller):
        tk.Frame.__init__(self,parent)

        """Add image to background"""
        # pick a .gif image file you have in the working directory
        image1 = tk.PhotoImage(file="ba.png")
        w = image1.width()
        h = image1.height()
        # tk.Frame has no image argument
        # so use a label as a panel/frame
        panel1 = tk.Label(self, image=image1)
        panel1.pack(side='top', fill='both', expand='yes')
        # save the panel's image from 'garbage collection'
        panel1.image = image1


        ##Red Button
        self.redButton = tk.Button(panel1, height = 10, width = 20,  bg="red", command = self.redButt)

        ##Blue Button
        self.blueButton = tk.Button(panel1, height = 10, width = 20,  bg="blue", command = self.blueButt)

        ##Green Button
        self.greenButton = tk.Button(panel1, height = 10, width = 20,  bg="green", command = self.greenButt)

        ##Yellow Button
        self.yellowButton = tk.Button(panel1, height = 10, width = 20,  bg="yellow", command = self.yellowButt)

        ##Start Game
        self.startButton = ttk.Button(panel1, text = "Start", command=self.start)

        ##Back to Main Menu
        menuButton = ttk.Button(panel1, text = "Go Back", command=lambda: combine_funcs(controller.show_frame(StartPage), self.reset()))
        menuButton.grid(row=2 , column = 0, sticky = "nsew")

        self.blueButton.place(x = 130, y = 144)
        self.redButton.place(x = 285, y = 144)
        self.greenButton.place(x = 130, y = 310)
        self.yellowButton.place(x = 285, y = 310)
        self.startButton.place(x = 480, y = 0)

    ##Start Button
    def start(self):
        global startButt
        global name
        ## if there is no game in progress get the users names and start a new game
        if startButt == False:
            name = tkinter.simpledialog.askstring("Name", "Enter your name:")
            ##Make sure the name variable is never empty
            if name == None:
                name = "AAA"
            sleep(.5)
            self.game()
        else:
            pass
        startButt = True
    
    ##Reset when returned to main menu
    def reset(self):
        ##reset all variables when returned to main menu or game ends
        global userCount
        global start
        global buttonNumber
        global gameList
        global startButt
        gameList = []
        userCount = 0
        start = 0
        buttonNumber = 5
        startButt = False
        
    ##adds random numbers to a list. Each number represents a button flash on the game board.
    def game(self):
        #red = 0
        #blue = 1
        #green = 2
        #yellow = 3
        global gameList
        global start
        global speed
        global userCount
        start = 1
        speed = .5
        
        ##change speed as player progresses
        for i in range(len(gameList)):
            speed -= .03
            ##Make sure speed is never negative
            if speed <= 0:
                speed = .001 
        
        ##creates a random number for the button flash
        color = randint(0,3)
        gameList.append(color)

        
        ## if the button number matches the random gamelist number then flash that button
        for i in range(0,len(gameList)):
            sleep(.2)
            if gameList[i] == 0:
                sleep(speed)
                self.redButton.flash()
            elif gameList[i] == 1:
                sleep(speed)
                self.blueButton.flash()
            elif gameList[i] == 2:
                sleep(speed)
                self.greenButton.flash()
            elif gameList[i] == 3:
                sleep(speed)
                self.yellowButton.flash()
            #print(speed)

            
    """Check the List of flashed buttons against the button clicked. Run game() again if length of the list
        equals the number of buttons clicked and all index values matched the users choices"""  
    def user(self):
        global userCount
        global start
        global buttonNumber
        global gameList

        ##Check if the users button press matches the random button numbers in the gameList
        if len(gameList) > 0:
            if gameList[userCount] == buttonNumber:
                userCount+=1
                buttonNumber = 5
                
            ##add user to high score file after completion of a game
            else:

                with open("Hi.txt", "a") as myfile:
                    myfile.write("\n" + name +" "+ str(len(gameList)-1))
                vart = tkinter.messagebox.showinfo("Game Over", "Game Over!" +"\n"+ "Your Score: " + str(len(gameList)-1))
                self.reset()
        ##if the user completes the round, call the game function again        
        if len(gameList) == userCount and start != 0:
            userCount = 0
            self.game()

        elif start == 0:
            vart = tkinter.messagebox.showinfo("Not so Fast!", "Press Start Button To Play")
    
            
    
    def redButt(self):
        global buttonNumber
        buttonNumber = 0
        self.user()
        
        #print(buttonNumber)
    def blueButt(self):
        global buttonNumber
        buttonNumber = 1
        self.user()
        
        #print(buttonNumber)
    def greenButt(self):
        global buttonNumber
        buttonNumber = 2
        self.user()
        
        #print(buttonNumber)
    def yellowButt(self):
        global buttonNumber
        buttonNumber = 3
        self.user()
        #print(buttonNumber)
        
##Rules Page
class Rules(tk.Frame):
    
    def __init__(self,parent,controller):
        tk.Frame.__init__(self,parent)

        """Add image to background"""
        # pick a .gif image file you have in the working directory
        image1 = tk.PhotoImage(file="ba.png")
        w = image1.width()
        h = image1.height()
        # tk.Frame has no image argument
        # so use a label as a panel/frame
        panel1 = tk.Label(self, image=image1)
        panel1.pack(side='top', fill='both', expand='yes')
        # save the panel's image from 'garbage collection'
        panel1.image = image1

        self.rules = tk.Label(panel1, text = "A game of Simon is played\n by lighting up one or more buttons\n in a random order, after which \nthe player must reproduce \nthat order by pressing the buttons.", font = ("Veradnda",24) )
        self.rules.pack()

        self.back = ttk.Button(panel1, text = "Back" ,command = lambda: controller.show_frame(StartPage) )
        self.back.pack()

##Funtion to combine other functions for use in multi function button commands
def combine_funcs(self,*funcs):
    def combined_func(self,*args, **kwargs):
        for f in funcs:
            f(*args, **kwargs)
    return combined_func


#run app
app = Window()
app.resizable(width=False, height=False)
app.mainloop()

        

